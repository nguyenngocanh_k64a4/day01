CREATE TABLE QLSV;
USE QLSV;

CREATE TABLE DMKHOA (
    MaKH varchar(6) NOT NULL PRIMARY KEY,
    TenKhoa varchar(30) NOT NULL
);

CREATE TABLE SINHVIEN (
    MaSV varchar(6) NOT NULL PRIMARY KEY,
    HoSV varchar(30),
    TenSV varchar(15),
    GioiTinh char(1),
    NgaySinh datetime,
    NoiSinh varchar(50),
    DiaChi varchar(50),
    MaKH varchar(6) FOREIGN KEY REFERENCES DMKHOA(MaKH),
    HocBong int
); 

SELECT *
FROM SINHVIEN
INNER JOIN DMKHOA ON SINHVIEN.MaKH = DMKHOA.MaKH
WHERE DMKHOA.TenKhoa="Công nghệ thông tin";
